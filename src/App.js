import React, { Component } from 'react';
//import logo from './logo.svg';

import 'bootstrap/dist/css/bootstrap.css';
import Home from './components/Home';
import Login from './components/Login';
import fire from './config/Auth';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: {},
			flag: false
		};
	}

	componentDidMount() {
		this.authListener();
	}

	authListener() {
		fire.auth().onAuthStateChanged((user) => {
			//console.log(user);
			if (user) {
				this.setState({ flag: true });
				console.log(user);
				this.setState({ user });
				//localStorage.setItem('user', user.uid);
			} else {
				this.setState({ flag: false });
				this.setState({ user: null });
				//localStorage.removeItem('user');
			}
		});
	}

	render() {
		return (
			<div>
				<div className="container">
					{!this.state.flag ? <Login /> : null}
					{this.state.flag ? <Home authuser={this.state.user} /> : null}
				</div>
			</div>
		);
	}
}

export default App;
