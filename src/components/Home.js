import React, { Component } from "react";
import fire from "../config/Auth";
class Home extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout() {
    fire.auth().signOut();
  }
  render() {
    if (this.props.authuser) {
      return (
        <div>
          <h1>You are in Home Page..</h1>
          <button onClick={this.logout} className="btn btn-danger btn-sm">
            SignOut
          </button>
        </div>
      );
    } else {
      return false;
    }
  }
}

export default Home;
