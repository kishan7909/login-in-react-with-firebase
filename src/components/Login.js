/* eslint-disable no-useless-escape */
import React, { Component } from 'react';
import fire from '../config/Auth';
class Login extends Component {
	constructor(props) {
		super(props);
		this.login = this.login.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.signup = this.signup.bind(this);
		this.state = {
			email: '',
			password: ''
		};
	}

	handleChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}

	login(e) {
		e.preventDefault();
		console.log('Login Click');
		fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u) => {}).catch((error) => {
			console.log(error);
		});
	}
	signup(e) {
		e.preventDefault();
		console.log('Sign Up Click');
		fire
			.auth()
			.createUserWithEmailAndPassword(this.state.email, this.state.password)
			.then((u) => {})
			.catch((error) => {
				console.log(error);
			});
	}

	render() {
		return (
			<form>
				<div className="row mt-5">
					<div className="col-md-6 offset-md-3">
						<div className="form-group">
							<label htmlFor="email">Email Address</label>
							<input
								type="text"
								name="email"
								placeholder="Email address"
								className="form-control"
								onChange={this.handleChange}
							/>
							<label style={{ color: 'red' }}>{this.state.email}</label>
							<br />
							<label htmlFor="password">Password</label>
							<input
								type="password"
								name="password"
								placeholder="Password"
								className="form-control"
								onChange={this.handleChange}
							/>
							<button type="submit" onClick={this.login} className="btn btn-primary mt-3">
								Login
							</button>
							<button onClick={this.signup} className="btn btn-success mt-3 ml-3">
								SignUp
							</button>
						</div>
					</div>
				</div>
			</form>
		);
	}
}

export default Login;
