import firebase from "firebase";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyBaxjJkm8V3KoyClHXKQrRebyRsS09hjh8",
  authDomain: "user-e9b30.firebaseapp.com",
  databaseURL: "https://user-e9b30.firebaseio.com",
  projectId: "user-e9b30",
  storageBucket: "user-e9b30.appspot.com",
  messagingSenderId: "641822994663"
};
const fire = firebase.initializeApp(config);
export default fire;
